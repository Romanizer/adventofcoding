// Rock     -> A X
// Paper    -> B Y
// Scissors -> C Z
fn main() {
    let games: Vec<String> = include_str!("../input").lines()
        .map( |x| x.parse().unwrap()).collect();

    let mut total_score = 0;

    for game in games.clone() {
        let score = match &*game {
            // Draw
            "A X" =>  1 + 3,
            "B Y" =>  2 + 3,
            "C Z" =>  3 + 3,
            // WIN
            "A Y" =>  2 + 6,
            "B Z" =>  3 + 6,
            "C X" =>  1 + 6,
            // Loose
            "A Z" =>  3,
            "B X" =>  1,
            "C Y" =>  2,
            &_ => 0,
        };

        total_score += score;

        println!("{}", score);


    }

    total_score = 0;

    // Answer to part one
    println!("Total Score: {}", total_score);

    for game in games {
        let score = match &*game {
            // Draw
            "A Y" =>  1 + 3,
            "B Y" =>  2 + 3,
            "C Y" =>  3 + 3,
            // WIN
            "A Z" =>  2 + 6,
            "B Z" =>  3 + 6,
            "C Z" =>  1 + 6,
            // Loose
            "A X" =>  3,
            "B X" =>  1,
            "C X" =>  2,
            &_ => 0,
        };

        total_score += score;

        println!("{}", score);


    }
    
    // Answer to part two
    println!("Total Score: {}", total_score)
}
