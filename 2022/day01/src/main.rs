use std::fs::File;
use std::io::{self, BufRead};

fn main() {
    // open file and read lines
    let file = File::open("input").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut elves: Vec<i32> = vec![0];

    let mut current_elf = 0;
    for line in lines {

        let l = line.as_ref().unwrap();

        // if empty line jump to next calories sum
        if l.eq("") {
            current_elf += 1;
            elves.push(0);
            println!("next Elf: {}", current_elf);
        }
        else {
            let val = l.parse::<i32>().unwrap();
            elves[current_elf] += val;
            println!("{:?}", line);
        }
    }

    current_elf = 0;

    for i in 0..elves.len() {

        if elves[i] > elves[current_elf] {
            current_elf = i;
        }
        println!("{}", format!("Elf {} carries {} calories", i, elves[i]));
    }

    // Answer to part one
    println!("Elf with highest Calorie count: {}", format!("{} -> {}", current_elf, elves[current_elf]));

    // sort elves by calorie count
    elves.sort();

    let mut sum = 0;
    
    // print top 3 elves and sum
    for i in 0..=2 {
        let s = format!("Elf {} carries {} calories.", i, elves[elves.len() - 1 - i]);
        println!("{:?}", s);
        sum += elves[elves.len() - 1 - i];
    }

    // Answer to part two
    println!("The top Elves carry {} together", sum);
}
